# Installation guide

### Config

To configure your webserver you must create a file named `.env` with the following inside:

```py
PORT=8080
SECRET="your secret"
```

replace `your secret` with... your secret.

### ShareX Setup

Stuff in red is what you will probably need to change. Only https if you have it enabled through a proxy like nginx or cloudflare.

![sharex1](https://i.imgur.com/WDOGYjQ.png)
![sharex2](https://i.imgur.com/QsQ1psY.png)
