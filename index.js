require('dotenv').config()

const fs = require('fs')
const express = require('express')
const multer = require('multer')
const shortid = require('shortid')

const app = express()
const storage = multer.memoryStorage()
const upload = multer({ storage })

if (!fs.existsSync('./uploads'))
  fs.mkdirSync('./uploads')
app.set('trust proxy', true)

app.post('/upload', upload.single('file'), (req, res) => {
  try {
    if (req.headers.authorization !== process.env.SECRET)
      return res.status(401).json({ message: 'unauthorized' })
    let [ ext ] = req.file.originalname.match(/(?:\.\w+)?$/)
    if (/^\.[^.]*$/.test(req.file.originalname))
      ext = ''
    let file = `${shortid.generate()}${ext || ''}`
    const d = new Date()
    const dFormatted = `${d.getDate().toString().padStart(2, 0)}/${(d.getMonth() + 1).toString().padStart(2, 0)}/${d.getFullYear() % 100} ${((d.getHours() % 12) || 12).toString().padStart(2, 0)}:${d.getMinutes().toString().padStart(2, 0)} ${d.getHours() < 12 ? 'AM' : 'PM'}`
    console.log(`[${dFormatted}] file recieved`)
    console.log(' '.repeat(dFormatted.length + 3), 'file:', file)
    console.log(' '.repeat(dFormatted.length + 3), 'hostname:', req.hostname)
    fs.writeFile(`./uploads/${file}`, req.file.buffer, (err) => {
      if (err) {
        res.status(500).json({ message: err.message })
        console.error(err)
      } else {
        let redirect = `/${file}`
        if (req.headers['x-alt-host'])
          redirect = req.headers['x-alt-host'] + redirect
        res.redirect(redirect)
      }
    })
  } catch (err) {
    res.status(500).json({ message: err.message })
    console.error(err)
  }
})

app.use((req, res, next) => {
  res.set('Access-Control-Allow-Origin', '*')
  next()
})
app.use(express.static('uploads'))

app.listen(process.env.PORT, (err) => {
  if (err) throw err
  else console.log('listening on port ' + process.env.PORT)
})
